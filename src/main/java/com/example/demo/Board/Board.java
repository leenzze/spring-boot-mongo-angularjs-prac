package com.example.demo.Board;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
@Data
@Accessors(chain = true)
public class Board {
    @Id
    private String id;

    private String title;

    private String author;

    private Date makeDate;

    private String hits;

    private String content;
}