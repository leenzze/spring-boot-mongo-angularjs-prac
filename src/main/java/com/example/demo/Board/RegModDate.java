package com.example.demo.Board;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * Created by RED on 2016-07-06.
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RegModDate
{
    private Date regDate;

    private Date modDate;

    public RegModDate ()
    {
    }

    public RegModDate (Date regDate)
    {
        this.regDate = regDate;
    }

    public RegModDate (Date regDate, Date modDate)
    {
        this.regDate = regDate;
        this.modDate = modDate;
    }

    public static RegModDate makeToday ()
    {
        Date date = new Date ();

        return new RegModDate (date, date);
    }
}
