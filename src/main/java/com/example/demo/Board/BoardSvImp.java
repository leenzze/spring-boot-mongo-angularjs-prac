package com.example.demo.Board;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BoardSvImp implements BoardSv{
    @Autowired
    private BoardRepo boardRepo;

    @Override
    public void insert(Board board) {
        boardRepo.save(board);
    }

    @Override
    public Board get(String id) {
        return boardRepo.findOne(id);
    }

//    @Override
//    public List<Board> get() {
//        return boardRepo.findAll();
//    }

    @Override
    public void delete(String id) {
        boardRepo.delete(id);
    }
}

