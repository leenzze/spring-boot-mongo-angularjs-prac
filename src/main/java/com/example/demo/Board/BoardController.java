package com.example.demo.Board;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BoardController {
    @Autowired
    private BoardRepo boardRepo;


    /**
     * 게시판 리스트
     * @return {board}
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public List<Board> list() {
        return boardRepo.findAll();
    }

    /**
     * 새로운 글 생성
     * @param brd
     */
    @PostMapping(value = "/boardnew")
    public void insert(Model model, @RequestBody Board brd) {
        boardRepo.save(brd);
        return;
    }

    @DeleteMapping ("/delete/{id}")
    public void delete (Model model, @PathVariable("id") String id)
    {
        boardRepo.delete(id);
        return ;
    }
}
