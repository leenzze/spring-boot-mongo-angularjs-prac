package com.example.demo.Board;

/**
 * Created by newopa on 2017. 2. 16..
 */
public interface BoardSv {

    void insert (Board board);

    Board get (String id);

    void delete (String id);

}
