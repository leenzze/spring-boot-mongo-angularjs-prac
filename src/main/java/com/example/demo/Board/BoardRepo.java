package com.example.demo.Board;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface BoardRepo extends MongoRepository<Board,String>{
}