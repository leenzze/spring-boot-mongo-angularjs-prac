package com.example.demo.Login;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
@Data
@Accessors(chain = true)
public class Login {
    @Id
    private String id;

    private String userId;

    private String userPw;

    private String userName;

    private String userEmail;

    private String userPhone;

    private String userTell;

    private Date regDate;
}
