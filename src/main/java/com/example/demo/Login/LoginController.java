package com.example.demo.Login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {
    @Autowired
    private LoginRepo loginRepo;

    @PostMapping(value = "/join")
    public void insert(Model model, @RequestBody Login lo) {
        loginRepo.save(lo);
        return;
    }

    @PostMapping(value = "/login")
    public boolean get(Model model, @RequestBody Login lo) {
        String userId = lo.getUserId();
        String userPw = lo.getUserPw();

        Login logindata = loginRepo.findByUserId(userId);

        if(logindata.getUserPw().equals(userPw)) return true;
        return false;
    }
}