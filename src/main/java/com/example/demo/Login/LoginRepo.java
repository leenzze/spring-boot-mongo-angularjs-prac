package com.example.demo.Login;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface LoginRepo extends MongoRepository<Login,String>{
    Login findByUserId(String userId);
}