package com.example.demo.Login;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LoginSvImp implements LoginSv{
    @Autowired
    private LoginRepo loginRepo;

    @Override
    public void insert(Login login) {
        loginRepo.save(login);
    }

    @Override
    public Login get(String id) {
        return loginRepo.findOne(id);
    }

}

