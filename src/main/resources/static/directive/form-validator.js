(function (app) {

/**
 * Enum for error code
 * @readonly
 * @enum {number}
 */
var ERROR_CODE = {
    /** 유효성 검사 통과 */
    NONE: 0,
    /** 값이 없을 경우 */
    REQUIRED: 1,
    /** 패턴 불일치 */
    PATTERN: 2,
    /** 최소값 보다 작다 */
    MIN: 3,
    /** 최대값 보다 크다 */
    MAX: 4,
    /** 값이 불일치 */
    SAME: 5
};

/**
 * 유효성 검사
 * @param {Object} scope
 * @param {Object} element
 * @param {Object} ngModel
 * @param {Function} validate 유효성 검사 통과 여부 반환
 * @param {Object} options 유효성 검사 옵션
 * @param {string} options.validation 유효성 검사 대상 이름
 * @param {string} options.focusOff 포커스 사용 여부 키 이름
 * @param {?string} options.compareSrc 비교 대상 이름
 */
var _validator = function (scope, element, ngModel, validate, options) {
    // 먼저 유효성 검사 여부를 통과 시킨다.
    ngModel.$setValidity('customValidity', false);

    /**
     * Focus 사용 여부
     * @type {boolean}
     * true: 항상 유효성 검사를 하겠다.
     * false: 포커스가 있을 때 부터 유효성 검사를 시작하겠다.
     */
    var flag = scope[options.focusOff] || false;

    /**
     * 유효성 검사 통과 여부에 따른 FORM 유효성 변경 및 UI 변경
     * @param {string} val 유효성 검사 대상
     */
    var validation = function (val) {
        if (validate(scope, val)) {
            ngModel.$setValidity('customValidity', false);
            element.css('border-color', '#FF4136');
        } else {
            ngModel.$setValidity('customValidity', true);
            element.css('border-color', '');
        }
    };

    // 포커스 이벤트를 등록
    element.bind('focus', function () {
        if (!flag) {
            flag = true;
            validation(ngModel.$modelValue);
        }
    });

    /**
     * Watch할 스코프 설정
     * @type {Array.<Function>}
     */
    var watchScope = [function () {
        return ngModel.$modelValue;
    }];

    if (options.compareSrc) {
        watchScope.push(options.compareSrc);
    }

    // Scope를 watch
    scope.$watchGroup(watchScope, function (newValue) {
        if (newValue[0] && !flag) {
            flag = true;
        }

        if (flag) {
            validation(newValue[0]);
        }
    });
};

/**
 * 유효성 검사 디렉티브의 공통 링크 함수
 * @param {Function} validate 유효성 검사 통과 여부 반환
 * @param {Object} options 유효성 검사 옵션
 * @param {string} options.validation 유효성 검사 대상 이름
 * @param {string} options.focusOff 포커스 사용 여부 키 이름
 */
var directiveLink = function (validate, options) {
    return function (scope, element, attrs, ngModel) {
        _validator(scope, element, ngModel, validate, options);
    };
};

// 아이디 유효성 검사
app.directive('idValidation', function () {
    console.log('gggg');
    /**
     * 아이디 pattern
     * @const {RegExp}
     */
    var ID_PATTERN = /^[a-z0-9][a-z0-9_\-]{4,19}$/;

    /**
     * 유효성 검사 옵션
     * @const {Object}
     */
    var options = {
        validation: 'idValidation',
        focusOff: 'idValidationFocusOff'
    };

    /**
     * 에러 여부 반환(값이 없거나, 패턴이 불일치 했을 때)
     * @param {Object} scope
     * @param {string} val 유효성 검사 대상
     */
    var validate = function (scope, val) {
        scope[options.validation] = val == undefined || val == '' ?
            ERROR_CODE.REQUIRED : !ID_PATTERN.test(val) ?
            ERROR_CODE.PATTERN : ERROR_CODE.NONE;
        return scope[options.validation] != ERROR_CODE.NONE;
    };

    return {
        require: 'ngModel',
        scope: {
            'idValidation': '=?',
            'idValidationFocusOff': '=?'
        },
        link: directiveLink(validate, options)
    };
});

// 비밀번호 유효성 검사
app.directive('passwordValidation', function () {

    /**
     * 비밀번호 최소 길이
     * @const {number}
     */
    var PW_MIN_SIZE = 4;

    /**
     * 유효성 검사 옵션
     * @const {Object}
     */
    var options = {
        validation: 'passwordValidation',
        focusOff: 'passwordValidationFocusOff'
    };

    /**
     * 에러 여부 반환(값이 없거나, 최소값보다 작을 때)
     * @param {Object} scope
     * @param {string} val 유효성 검사 대상
     */
    var validate = function (scope, val) {
        scope[options.validation] = val == undefined || val == '' ?
            ERROR_CODE.REQUIRED : val.length < PW_MIN_SIZE ?
            ERROR_CODE.MIN : ERROR_CODE.NONE;
        return scope[options.validation] != ERROR_CODE.NONE;
    };

    return {
        require: 'ngModel',
        scope: {
            'passwordValidation': '=?',
            'passwordValidationFocusOff': '=?'
        },
        link: directiveLink(validate, options)
    };
});

// 이름 유효성 검사
app.directive('nameValidation', function () {

    /**
     * 이름 최대 길이
     * @const {number}
     */
    var NAME_MAX_SIZE = 20;

    /**
     * 유효성 검사 옵션
     * @const {Object}
     */
    var options = {
        validation: 'nameValidation',
        focusOff: 'nameValidationFocusOff'
    };

    /**
     * 에러 여부 반환(값이 없거나, 최대값보다 클 때)
     * @param {Object} scope
     * @param {string} val 유효성 검사 대상
     */
    var validate = function (scope, val) {
        scope[options.validation] = val == undefined || val == '' ?
            ERROR_CODE.REQUIRED : val.length >= NAME_MAX_SIZE ?
            ERROR_CODE.MAX : ERROR_CODE.NONE;
        return scope[options.validation] != ERROR_CODE.NONE;
    };

    return {
        require: 'ngModel',
        scope: {
            'nameValidation': '=?',
            'nameValidationFocusOff': '=?'
        },
        link: directiveLink(validate, options)
    };
});

// 이메일 유효성 검사
app.directive('emailValidation', function () {

    /**
     * 이메일 pattern
     * @const {RegExp}
     */
    var EMAIL_PATTERN = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;

    /**
     * 유효성 검사 옵션
     * @const {Object}
     */
    var options = {
        validation: 'emailValidation',
        focusOff: 'emailValidationFocusOff'
    };

    /**
     * 에러 여부 반환(값이 없거나, 패턴이 불일치 했을 때)
     * @param {Object} scope
     * @param {string} val 유효성 검사 대상
     */
    var validate = function (scope, val) {
        scope[options.validation] = val == undefined || val == '' ?
            ERROR_CODE.REQUIRED : !EMAIL_PATTERN.test(val) ?
            ERROR_CODE.PATTERN : ERROR_CODE.NONE;
        return scope[options.validation] != ERROR_CODE.NONE;
    };

    return {
        require: 'ngModel',
        scope: {
            'emailValidation': '=?',
            'emailValidationFocusOff': '=?'
        },
        link: directiveLink(validate, options)
    };
});

// 휴대폰 번호 유효성 검사
app.directive('phoneValidation', function () {

    /**
     * 휴대폰 번호 pattern
     * @const {RegExp}
     */
    var PHONE_PATTERN = /^\d{3}-?\d{3,4}-?\d{4}$/;

    /**
     * 유효성 검사 옵션
     * @const {Object}
     */
    var options = {
        validation: 'phoneValidation',
        focusOff: 'phoneValidationFocusOff'
    };

    /**
     * 에러 여부 반환(값이 없거나, 패턴이 불일치 했을 때)
     * @param {Object} scope
     * @param {string} val 유효성 검사 대상
     */
    var validate = function (scope, val) {
        scope[options.validation] = val == undefined || val == '' ?
            ERROR_CODE.REQUIRED : !PHONE_PATTERN.test(val) ?
            ERROR_CODE.PATTERN : ERROR_CODE.NONE;
        return scope[options.validation] != ERROR_CODE.NONE;
    };

    return {
        require: 'ngModel',
        scope: {
            'phoneValidation': '=?',
            'phoneValidationFocusOff': '=?'
        },
        link: directiveLink(validate, options)
    };
});

// 연락처 유효성 검사
app.directive('homeValidation', function () {

    /**
     * 연락처 pattern
     * @const {RegExp}
     */
    var HOME_PATTERN = /^\d{2,3}-?\d{3,4}-?\d{4}$/;

    /**
     * 유효성 검사 옵션
     * @const {Object}
     */
    var options = {
        validation: 'homeValidation',
        focusOff: 'homeValidationFocusOff'
    };

    /**
     * 에러 여부 반환(값이 없거나, 패턴이 불일치 했을 때)
     * @param {Object} scope
     * @param {string} val 유효성 검사 대상
     */
    var validate = function (scope, val) {
        scope[options.validation] = val == undefined || val == '' ?
            ERROR_CODE.REQUIRED : !HOME_PATTERN.test(val) ?
            ERROR_CODE.PATTERN : ERROR_CODE.NONE;
        return scope[options.validation] != ERROR_CODE.NONE;
    };

    return {
        require: 'ngModel',
        scope: {
            'homeValidation': '=?',
            'homeValidationFocusOff': '=?'
        },
        link: directiveLink(validate, options)
    };
});

// 필수 유효성 검사
app.directive('requiredValidation', function () {

    /**
     * 유효성 검사 옵션
     * @const {Object}
     */
    var options = {
        validation: 'requiredValidation',
        focusOff: 'requiredValidationFocusOff'
    };

    /**
     * 에러 여부 반환(값이 없을 때)
     * @param {Object} scope
     * @param {string} val 유효성 검사 대상
     */
    var validate = function (scope, val) {
        scope[options.validation] = val == undefined || val == '' ?
            ERROR_CODE.REQUIRED : ERROR_CODE.NONE;
        return scope[options.validation] != ERROR_CODE.NONE;
    };

    return {
        require: 'ngModel',
        scope: {
            'requiredValidation': '=?',
            'requiredValidationFocusOff': '=?'
        },
        link: directiveLink(validate, options)
    };
});

// 비교 유휴성 검사
app.directive('confirmValidation', function () {

    /**
     * 유효성 검사 옵션
     * @const {Object}
     */
    var options = {
        validation: 'confirmValidation',
        focusOff: 'confirmValidationFocusOff',
        compareSrc: 'confirmValidationSrc'
    };

    /**
     * 에러 여부 반환(비교값과 불일치 했을 때)
     * @param {Object} scope
     * @param {string} val 유효성 검사 대상
     */
    var validate = function (scope, val) {
        scope[options.validation] = val == undefined || val == '' ?
            ERROR_CODE.REQUIRED : scope[options.compareSrc] != val ?
            ERROR_CODE.SAME : ERROR_CODE.NONE;
        return scope[options.validation] != ERROR_CODE.NONE;
    };

    return {
        require: 'ngModel',
        scope: {
            'confirmValidation': '=?',
            'confirmValidationFocusOff': '=?',
            'confirmValidationSrc': '=?'
        },
        link: directiveLink(validate, options)
    };
});

})(App);
