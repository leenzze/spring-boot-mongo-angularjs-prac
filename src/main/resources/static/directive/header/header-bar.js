/**
 * Created by leenzze on 2017-05-12.
 */
App.directive('headerBar', function($location) {
    return {
        restrict: 'AE',
        templateUrl: 'directive/header/header-bar.html',
        scope: {
            title: '=',
            content: '='
        },
        link: function(scope) {

            scope.title = scope.title;

            scope.content = scope.content;

            scope.logout = function() {
                sessionStorage.removeItem('userId');
                $location.path("/");
            };
        }
    }
});