/**
 * Created by newopa on 2017-05-10.
 */
App.factory('utilService', function() {
    /**
     * 10미만 정수라면 '0'을 붙여 반환
     * @type {number} n 원본 값
     * @return {string} 변환된 문자열
     */
    var addZero = function (n) {
        return n < 10 && n >= 0 ? '0' + n : '' + n;
    }

    return {
        addZero: addZero,

        /**
         * 날짜를 문자열로 포맷 지정 변경
         * @type {Date} d 날짜
         * @type {string} f 포맷
         * @return {string} formatted date
         */
        dateFormat: function (d, f) {
            if (!d.valueOf()) return ' ';
            var weekName = ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일'];
            var h;

            return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function ($1) {
                switch ($1) {
                    case 'yyyy':
                        return d.getFullYear();
                    case 'yy':
                        return addZero((d.getFullYear() % 1000));
                    case 'MM':
                        return addZero(d.getMonth() + 1);
                    case 'dd':
                        return addZero(d.getDate());
                    case 'E':
                        return weekName[d.getDay()];
                    case 'HH':
                        return addZero(d.getHours());
                    case 'hh':
                        return addZero(((h = d.getHours() % 12) ? h : 12));
                    case 'mm':
                        return addZero(d.getMinutes());
                    case 'ss':
                        return addZero(d.getSeconds());
                    case 'a/p':
                        return d.getHours() < 12 ? '오전' : '오후';
                    default:
                        return $1;
                }
            });
        }
    }
});