/**
 * Created by leenzze on 2017-05-10.
 */
App.controller("modalService", function($scope, close, title, content) {
    $scope.title = title;
    $scope.content = content;

    $scope.close = function(result) {
        close(result);
    };
});