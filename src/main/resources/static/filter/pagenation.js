/**
 * 리스트에서 분리할 갯수에 따라 분리하여 리턴함
 * @param {input} 리스트
 *        {start} 분리할 갯수
 * @return {input} 분리된 리스트
 */
App.filter('pagination', function() {
    return function(input, start) {
        if (input || start) return input.slice(start);
    };
});