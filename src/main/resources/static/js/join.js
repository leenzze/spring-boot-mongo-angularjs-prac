/**
 * Created by leenzze on 2017-05-11.
 */
App.controller('joinCtrl', function($scope, $http, $location, ModalService){
    'use strict';

    /**
     * 회원 데이터
     */
    $scope.memberData = {
        userId : '',
        userPw : '',
        userName : '',
        userEmail : '',
        userPhone : '',
        userTell : '',
        regDate : new Date()
    };

    /**
     * 암호화전 비밀번호
     */
    $scope.pw = '';

    /**
     * 회원 가입
     */
    $scope.submit = function() {
        $scope.memberData.userPw = md5($scope.pw);

        $http.post('/join', $scope.memberData).then(function(res){
            ModalService.showModal({
                templateUrl: "service/modal/modal-confirm.html",
                controller: "modalService",
                inputs : {
                    title : [
                        '00000의 멤버가 되신 것을',
                        '환영합니다!'],
                    content : '짝짝짞'}
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function() {
                    $location.path("/");
                });
            });
        }, function(error) {
            console.log('error', error);
            alert('오류발생. 관리자에게 문의하세용');
        });
    }

    /**
     * 취소
     */
    $scope.goback = function() {
        history.back(-1);
    };
});
