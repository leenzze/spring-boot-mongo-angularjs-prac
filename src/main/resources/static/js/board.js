/**
 * Created by leenzze on 2017-05-04.
 */
App.controller('boardCtrl', function ($scope, $http, $location, utilService, $window, ModalService) {
    'use strict';

    /**
     * 현재 페이지
     */
    $scope.currPage = 1;

    /**
     * 표현할 페이지 수
     */
    $scope.perPage = 10;

    /**
     * 게시글 번호
     */
    $scope.no = 0;

    $scope.$watch('currPage', function(newVal, oldVal) {
       if(newVal) paging();
    });

    /**
     * 헤더바 제목
     */
    $scope.title = '게시판';

    /**
     * 헤더바 설명
     */
    $scope.content = '암거나써 다써 그냥 막 써 게시판이야 막써';

    /**
     * 게시판 리스트 요청
     */
    function getList() {
       $http.get('/list').then(function (res) {
           $scope.boardList = res.data;
           $scope.brdLength = res.data.length;
           paging();
       }, function(err) {
           console.log('err',err);
       });
   }
   getList();

    function paging() {
        $scope.no = $scope.brdLength - ($scope.currPage - 1) * $scope.perPage;
    }

    /**
     * 게시판 삭제
     * @param {id} 게시판 글 아이디
     */
    $scope.delete = function(id) {
        ModalService.showModal({
            templateUrl: "service/modal/modal-service.html",
            controller: "modalService",
            inputs : {
                title : [
                    '정말로 삭제하시겠습니까?'],
                content : '삭제 후 되돌릴 수 없습니다.'}
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                if(result == 0) {
                    $http.delete('/delete/' + id).then(function(res) {
                        console.log(res);
                    }, function(err) {
                        console.log(err);
                    });
                    $window.location.reload();
                }
            });
        });
    };

    /**
     * 게시글 작성으로 이동
     */
    $scope.goWrite = function() {
        $location.path("/write");
    };
});
