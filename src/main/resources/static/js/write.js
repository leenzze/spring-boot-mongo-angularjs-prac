/**
 * Created by leenzze on 2017-05-11.
 */
App.controller('writeCtrl', function($scope, $http, $location){
    'use strict';

    /**
     * 게시판 데이터
     */
    $scope.boardData = {
        title : '',
        author : '',
        content : '',
        makeDate : new Date(),
        hits : 0
    };

    /**
     * 헤더바 제목
     */
    $scope.title = '게시판';

    /**
     * 헤더바 설명
     */
    $scope.content = '암거나써 다써 그냥 막 써 게시판이야 막써';

    $scope.submitForm = function() {
        $http.post('/boardnew', $scope.boardData).then(function(res){
            console.log(res);
            $location.path("/board");
        }, function(error) {
            console.log('error', error);
        });
    }
});