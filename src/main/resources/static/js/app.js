/**
 * Created by leenzze on 2017-05-04.
 */
var App = angular.module('demoApp', ['ngRoute','angularModalService', 'ui.bootstrap']);

App.config(function ($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: '../views/main.html',
        controller: 'mainCtrl'
    }).when('/join', {
        templateUrl: '../views/join.html',
        controller: 'joinCtrl'
    }).when('/board', {
        templateUrl: '../views/board.html',
        controller: 'boardCtrl'
    }).when('/write', {
        templateUrl: '../views/write.html',
        controller: 'writeCtrl'
    }).otherwise({
        redirectTo: '/'
    });
});
