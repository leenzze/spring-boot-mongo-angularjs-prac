/**
 * Created by leenzze on 2017-05-04.
 */
App.controller('mainCtrl', function ($rootScope, $scope, $location, $http, ModalService) {
    'use strict';

    /**
     * 알림 메시지 타입
     */
    var Alert = {
        /** 아이디와 비밀번호가 일치 하지 않음 */
        FAIL: 0,
        /** 서버에 연결 할 수 없음 */
        CONN: 1,
        /** 서버측 에러 */
        SERVER: 2
    };

    /**
     * 로그인 폼 모델
     */
    $scope.login = {
        userId: '',
        userPw: ''
    };

    /**
     * 암호화 전 비밀번호
     */
    $scope.pw = '';

    /**
     * 로그인 요청
     */
    $scope.doLogin = function() {
        $scope.login.userPw = md5($scope.pw);

        $http.post('/login', $scope.login).then(function(res){
            console.log(res);
            if (res.data) {
                sessionStorage.setItem('userId', $scope.login.userId);
                $location.path("/board");
            }
            else $scope.alert = Alert.FAIL;

        }, function(error) {
            console.log('error', error);
            $scope.alert = Alert.SERVER;
        });
    }

    /**
     * 회원가입으로 이동
     */
    $scope.goJoin = function() {
        $location.path("/join");
    };

    /**
     * 아이디/비밀번호찾기로 이동
     */
    $scope.findIdPw = function() {
        ModalService.showModal({
            templateUrl: "service/modal/findidpw/findidpw.html",
            controller: "findIdPwModalService"
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
               console.log('close', result);
            });
        });
    };
});